import matplotlib.pyplot as plt
import numpy as np

plt.title('Drone movement')
plt.axis([-2, 2, -2, 2])

fo = open("data_pos.txt", "r")

plt.gca().invert_yaxis()


while(True):
    line = fo.readline()
    if (line != ''):
        x = float(line.split()[0])
        y = float(line.split()[1])
        plt.scatter(y, x, c="blue")
        plt.pause(0.01)

plt.show()
fo.close()