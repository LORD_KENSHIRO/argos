/*
 * AUTHORS: Carlo Pinciroli <cpinciro@ulb.ac.be>
 *          Pierre-Yves Lajoie <lajoie.py@gmail.com>
 *          Gergi Younis <gergi.younis@polymtl.ca>
 *
 * An example crazyflie drones sensing.
 *
 * This controller is meant to be used with the XML file:
 *    experiments/foraging.argos
 */

#ifndef CRAZYFLIE_SENSING_H
#define CRAZYFLIE_SENSING_H

#define STEP 0.1 // Drone step movement
#define ALTITUDE_1 0.15 // First drone flight altitude
#define ALTITUDE_2 0.30 // Second drone flight altitude
#define ALTITUDE_3 0.45 // Third drone flight altitude
#define ALTITUDE_4 0.60 // Fourth drone flight altitude
#define CLOSEST_DISTANCE_TO_OBSTACLE 20
#define TOLERANCE 0.01
#define SPEED_X speed[0]
#define SPEED_Y speed[1]
#define TIME_INTERVAL 1
/*
 * Include some necessary headers.
 */
/* Definition of the CCI_Controller class. */
#include <argos3/core/control_interface/ci_controller.h>
/* Definition of the crazyflie distance sensor */
#include <argos3/plugins/robots/crazyflie/control_interface/ci_crazyflie_distance_scanner_sensor.h>
/* Definition of the crazyflie position actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_quadrotor_position_actuator.h>
/* Definition of the crazyflie position sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>
/* Definition of the crazyflie range and bearing actuator */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
/* Definition of the crazyflie range and bearing sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>
/* Definition of the crazyflie battery sensor */
#include <argos3/plugins/robots/generic/control_interface/ci_battery_sensor.h>
/* Definitions for random number generation */
#include <argos3/core/utility/math/rng.h>
/* Definition of file streams */
#include <fstream>

/*
 * All the ARGoS stuff in the 'argos' namespace.
 * With this statement, you save typing argos:: every time.
 */
using namespace argos;

/*
 * A controller is simply an implementation of the CCI_Controller class.
 */
class CCrazyflieSensing : public CCI_Controller {

public:

   /* Class constructor. */
   CCrazyflieSensing();
   /* Class destructor. */
   virtual ~CCrazyflieSensing() {}

   /*
    * This function initializes the controller.
    * The 't_node' variable points to the <parameters> section in the XML
    * file in the <controllers><footbot_foraging_controller> section.
    */
   virtual void Init(TConfigurationNode& t_node);

   /*
    * This function is called once every time step.
    * The length of the time step is set in the XML file.
    */
   virtual void ControlStep();

   /*
    * This function resets the controller to its state right after the
    * Init().
    * It is called when you press the reset button in the GUI.
    */
   virtual void Reset();

   /*
    * Called to cleanup what done by Init() when the experiment finishes.
    * In this example controller there is no need for clean anything up,
    * so the function could have been omitted. It's here just for
    * completeness.
    */
   virtual void Destroy() {}


   /* Get drone average speed */
   virtual void getSpeed();

   virtual void toBase();


private:

   /* Pointer to the crazyflie distance sensor */
   CCI_CrazyflieDistanceScannerSensor* m_pcDistance;

    /* Pointer to the position actuator */
   CCI_QuadRotorPositionActuator* m_pcPropellers;
   
   /* Pointer to the range and bearing actuator */
   CCI_RangeAndBearingActuator*  m_pcRABA;

   /* Pointer to the range and bearing sensor */
   CCI_RangeAndBearingSensor* m_pcRABS;

   /* Pointer to the positioning sensor */
   CCI_PositioningSensor* m_pcPos;

   /* Pointer to the battery sensor */
   CCI_BatterySensor* m_pcBattery;

   /* The random number generator */
   CRandom::CRNG* m_pcRNG;

   /* Current step */
   uint m_uiCurrentStep;

   /* Drone states */
   enum droneStates {
      MOVE_FORWARD, DECELERATE, ROTATE, STABILIZE_TRANSLATION, STABILIZE_ROTATION, RETURN_TO_BASE, LAND, WAITING_CMD
   };

    /* Drone current state */
   uint currentState;

   // Drone current position
   CVector3 cPos;

   // Drone previous position
   CVector3 cPrevPos;

   // Drone target and rotation angle
   Real targetAngle;
   Real rotAngle;

   // Drone XY speed
   Real speed[2];

   // Time-Position values
   std::vector<Real> stepPos;

   // Create a map of several angles of rotation to choose randomly from 
    std::map<int, CRadians> rotationAnglesRad; // Angles in radians
    std::map<int, Real> rotationAnglesDeg; // Angles in degrees

   // Retrieve drone data
    std::ofstream dronePosition;
    std::ofstream rangingDeck_1;
    std::ofstream rangingDeck_2;
    std::ofstream rangingDeck_3;
    std::ofstream rangingDeck_4;

};

#endif